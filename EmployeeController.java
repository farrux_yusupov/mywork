/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package uz.yt.fkadr.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import uz.yt.fkadr.model.Employee;
import uz.yt.fkadr.service.EmployeeService;

/**
 * created at: 16.11.2020
 * @author Yusupov Farrux <fyusupov90@gmail.com>
 */
@Controller
@RequestMapping("/employeelist")
public class EmployeeController {
    
    @Autowired
    EmployeeService  employeeService;
    
    
    @RequestMapping(name = "/list", method = RequestMethod.GET)
    public ModelAndView listOfEmployee(){
      Map<String, Object> model = new HashMap<String, Object>();
      model.put("employees",  employeeService.getAllEmployeeList());
      return new ModelAndView("table", model);
    }
    @RequestMapping(value = "/client_grid", method = RequestMethod.POST)
    public @ResponseBody HashMap<String, Object> taxDecisionList(Locale locale, HttpServletRequest request, HttpServletResponse response) {
        
        HashMap<String, Object> resMap = new LinkedHashMap<String, Object>();
        LocaleResolver localeResolver = RequestContextUtils.getLocaleResolver(request);
        String lang = localeResolver.resolveLocale(request).getLanguage();
        try {
            List<Employee> mainList = new ArrayList<>();
            mainList = employeeService.getAllEmployeeList();

            resMap.put("success", true);
            resMap.put("data", mainList);
        } catch (Exception e) {
            resMap.put("success", false);
            resMap.put("reason", e.getMessage());
        }
        return resMap;
    }
}
